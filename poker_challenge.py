from collections import Counter
from functools import total_ordering
from itertools import takewhile
from operator import itemgetter
import sys

# Hand Types
STRAIGHT_FLUSH = 9
FOUR_OF_A_KIND = 8
FULL_HOUSE = 7
FLUSH = 6
STRAIGHT = 5
THREE_OF_A_KIND = 4
TWO_PAIR = 3
PAIR = 2
HIGH_CARD = 1

@total_ordering
class Cards:

    cards = []

    def __init__(self, cardstr):

        def map_rank(rank):
            rank_map = {'A': 14, 'K': 13, 'Q': 12, 'J':11}
            if rank in rank_map:
                return rank_map[rank]
            return int(rank)

        self.cards = [card.strip() for card in cardstr.split(",")]            # split the cards into a list of strings, removing extra whitespace
        self.cards = [(map_rank(card[:-1]), card[-1]) for card in self.cards] # split each card in the list into a tuple with separate rank and suit

    def is_straight_flush(self):
        return self.is_straight() and self.is_flush()

    def is_four_of_a_kind(self):
        return self.__get_most_common_cards()[0][1] >= 4     # get_most_common_cards returns a list containing tuples, [0][1] selects the count of that most common item

    def is_full_house(self):
        return self.__get_most_common_cards()[0][1] == 3 and self.__get_most_common_cards()[1][1] == 2

    def is_flush(self):
        suits = [card[1] for card in self.cards]
        return len(set(suits)) == 1

    def is_straight(self):
        ranks = self.__get_sorted_ranks()
        for ix, val in enumerate(ranks[:-1]):
            if val != ranks[ix+1] + 1:
                return False
        return True

    def is_three_of_a_kind(self):
        return self.__get_most_common_cards()[0][1] >= 3     # get_most_common_cards returns a list containing tuples, [0][1] selects the count of that most common item

    def is_two_pair(self):
        return self.__get_most_common_cards()[0][1] >= 2 and self.__get_most_common_cards()[1][1] >= 2

    def is_pair(self):
        return self.__get_most_common_cards()[0][1] >= 2     # get_most_common_cards returns a list containing tuples, [0][1] selects the count of that most common item

    def classify_hand(self):
        if self.is_straight_flush():
            return STRAIGHT_FLUSH
        if self.is_four_of_a_kind():
            return FOUR_OF_A_KIND
        if self.is_full_house():
            return FULL_HOUSE
        if self.is_flush():
            return FLUSH
        if self.is_straight():
            return STRAIGHT
        if self.is_three_of_a_kind():
            return THREE_OF_A_KIND
        if self.is_two_pair():
            return TWO_PAIR
        if self.is_pair():
            return PAIR
        return HIGH_CARD

    def __get_sorted_ranks(self):
        return sorted([card[0] for card in self.cards], reverse=True)

    def __get_most_common_cards(self):
        counts = Counter(self.__get_sorted_ranks()).most_common()  # for cards that occur the same amount of times, most_common returns them in arbitrary order
        counts.sort(key=itemgetter(0), reverse=True)               # first sort by card rank so that greater ranks are first
        counts.sort(key=itemgetter(1), reverse=True)               # then sort by most common element.  sort() is a stable sort.
        return counts

    def __compare(self, other):
        # First, order by the hand type
        self_hand_type = self.classify_hand()
        other_hand_type = other.classify_hand()

        if self_hand_type != other_hand_type:
            return self_hand_type - other_hand_type

        # If both are the same hand type, we'll do some further comparison depending on the type
        if self_hand_type == FLUSH or self_hand_type == HIGH_CARD or self_hand_type == STRAIGHT_FLUSH or self_hand_type == STRAIGHT:
            return cmp(self.__get_sorted_ranks(), other.__get_sorted_ranks())

        elif self_hand_type == FOUR_OF_A_KIND or self_hand_type == THREE_OF_A_KIND or self_hand_type == PAIR:

            def compare_group(group_size):
                # for three of a kind/pair, first we'll compare the matching cards from each hand, if they are different then the hands are different
                self_group_card = self.__get_most_common_cards()[0][0]
                other_group_card = other.__get_most_common_cards()[0][0]

                # if the matching cards are the same, look at the cards which are left over to see if one has a higher "kicker"
                # this fails if we are treating a four of a kind as a three of a kind (for example), but classify_hand shouldn't do that now
                self_ranks = [x for x in self.__get_sorted_ranks() if x != self_group_card]
                other_ranks = [x for x in other.__get_sorted_ranks() if x != other_group_card]

                return cmp([self_group_card] + self_ranks, [other_group_card] + other_ranks)

            if self_hand_type == FOUR_OF_A_KIND:
                return compare_group(4)
            if self_hand_type == THREE_OF_A_KIND:
                return compare_group(3)
            elif self_hand_type == PAIR:
                return compare_group(2)

        elif self_hand_type == TWO_PAIR:
            # get the first and second pair ranks in each hand, and the leftover card
            self_first_pair_card = self.__get_most_common_cards()[0][0]
            other_first_pair_card = other.__get_most_common_cards()[0][0]

            self_second_pair_card = self.__get_most_common_cards()[1][0]
            other_second_pair_card = other.__get_most_common_cards()[1][0]

            self_ranks = [x for x in self.__get_sorted_ranks() if x != self_first_pair_card and x != self_second_pair_card]
            other_ranks = [x for x in other.__get_sorted_ranks() if x != other_first_pair_card and x != other_second_pair_card]

            return cmp([self_first_pair_card, self_second_pair_card] + self_ranks,
                       [other_first_pair_card, other_second_pair_card] + other_ranks)

        elif self_hand_type == FULL_HOUSE:
            self_counts = self.__get_most_common_cards()
            other_counts = other.__get_most_common_cards()

            # compare two tuples based on the two ranks in the full house
            return cmp( (self_counts[0][0], self_counts[1][0]), (other_counts[0][0], other_counts[1][0]) )

        raise RuntimeError("Unknown hand type in __compare: " + str(self_hand_type))

    def __eq__(self, other):
        return self.__compare(other) == 0

    def __ne__(self, other):
        return not (self == other)

    def __gt__(self, other):
        return self.__compare(other) > 0

def parse_player(player_str):
    player, cards = player_str.split(",", 1)   # split the player name off the front of the input
    return (player, Cards(cards))              # create a Cards class and return it in a tuple with the player name

def parse_players(player_str_list):
    return map(parse_player, player_str_list)

def find_winners(input_list):
    player_list = parse_players(input_list)
    player_list.sort(key=lambda x: x[1], reverse=True)
    first_winner = player_list[0]

    return [player[0] for player in player_list if player[1] == first_winner[1]]

def main():
    input = sys.stdin.readlines()
    winners = find_winners(input)
    for player in winners:
        print player

if __name__ == '__main__':
    main()
