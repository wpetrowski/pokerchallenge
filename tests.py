import unittest
import poker_challenge as pc

class PokerTests(unittest.TestCase):

    def test_parse_player(self):
        expectedName = "Joe"
        actual = pc.parse_player("Joe, 3H, 4H, 5H, 6H, 8H")
        self.assertEqual(expectedName, actual[0])

class CardParseTests(unittest.TestCase):

    def test_parse_cards_two_digit_card_rank(self):
        expected = [(10, "H"), (4, "H"), (5, "H"), (6, "H"), (8, "H")]
        actual = pc.Cards("10H, 4H, 5H, 6H, 8H")
        self.assertEqual(expected, actual.cards)

    def test_parse_cards_with_AKQJ_cards(self):
        expected = [(14, "C"), (13, "C"), (12, "C"), (11, "S"), (9, "C")]
        actual = pc.Cards("AC, KC, QC, JS, 9C")
        self.assertEqual(expected, actual.cards)

class HandDetectionTests(unittest.TestCase):

    def test_is_flush(self):
        input = "3H, 4H, 5H, 6H, 8H"
        self.assertTrue(pc.Cards(input).is_flush())

    def test_is_not_flush(self):
        input = "3H, 4S, 5H, 6C, 8H"
        self.assertFalse(pc.Cards(input).is_flush())

    def test_diamonds_flush(self):
        input = "9D, 8D, 5D, 6D, 7D"
        self.assertTrue(pc.Cards(input).is_flush())

    def test_is_three_of_a_kind(self):
        input = "2H, 5H, 2S, 2D, 7C"
        self.assertTrue(pc.Cards(input).is_three_of_a_kind())

    def test_four_of_a_kind_counts_as_three(self):
        input = "3H, 5H, 3S, 3D, 3C"
        self.assertTrue(pc.Cards(input).is_three_of_a_kind())

    def test_not_three_of_a_kind(self):
        input = "3H, 4H, 6S, 6D, 7C"
        self.assertFalse(pc.Cards(input).is_three_of_a_kind())

    def test_is_pair(self):
        input = "9S, 10D, 4S, 9C, 3H"
        self.assertTrue(pc.Cards(input).is_pair())

    def test_is_not_pair(self):
        input = "2S, 3D, 4S, 5C, 6H"
        self.assertFalse(pc.Cards(input).is_pair())

    def test_three_of_a_kind_counts_as_pair(self):
        input = "2H, 5H, 2S, 2D, 7C"
        self.assertTrue(pc.Cards(input).is_pair())

    def test_four_of_a_kind_counts_as_pair(self):
        input = "3H, 5H, 3S, 3D, 3C"
        self.assertTrue(pc.Cards(input).is_pair())

    def test_two_pair_counts_as_pair(self):
        input = "3H, 3C, 4S, 4D, 5C"
        self.assertTrue(pc.Cards(input).is_pair())

    def test_full_house_counts_as_pair(self):
        input = "3H, 3C, 4S, 4D, 3D"
        self.assertTrue(pc.Cards(input).is_pair())

    def test_is_straight_flush(self):
        input = "9H, 10H, QH, JH, 8H"
        self.assertTrue(pc.Cards(input).is_straight_flush())

    def test_is_straight_flush_just_flush(self):
        input = "9H, 10H, QH, JH, 2H"
        self.assertFalse(pc.Cards(input).is_straight_flush())

    def test_is_straight_flush_just_straight(self):
        input = "9H, 10C, QD, JH, 8H"
        self.assertFalse(pc.Cards(input).is_straight_flush())

    def test_is_straight(self):
        input = "3H, 6D, 5C, 4C, 7C"
        self.assertTrue(pc.Cards(input).is_straight())

    def test_is_not_straight(self):
        input = "3H, 6D, 8C, 4C, 7C"
        self.assertFalse(pc.Cards(input).is_straight())

        input = "3H, 6D, 5C, 4C, AC"
        self.assertFalse(pc.Cards(input).is_straight())

        input = "3H, 6D, 5C, 4C, 4H"
        self.assertFalse(pc.Cards(input).is_straight())

    def test_is_not_straight_ace_is_not_low(self):
        input = "AC, 2D, 3D, 4C, 5H"
        self.assertFalse(pc.Cards(input).is_straight())

    def test_is_four_of_a_kind(self):
        input = "JH, JC, JS, JD, AS"
        self.assertTrue(pc.Cards(input).is_four_of_a_kind())

    def test_is_not_four_of_a_kind(self):
        input = "JH, JC, JS, AD, AS"
        self.assertFalse(pc.Cards(input).is_four_of_a_kind())

    def test_is_full_house(self):
        input = "JH, JC, JS, AD, AS"
        self.assertTrue(pc.Cards(input).is_full_house())

    def test_is_not_full_house(self):
        input = "JH, JC, QS, AD, AS"
        self.assertFalse(pc.Cards(input).is_full_house())

    def test_is_two_pair(self):
        input = "JH, JC, QS, AD, AS"
        self.assertTrue(pc.Cards(input).is_two_pair())

    def test_full_house_counts_as_two_pair(self):
        input = "JH, JC, AC, AD, AS"
        self.assertTrue(pc.Cards(input).is_two_pair())

    def test_is_not_two_pair(self):
        input = "4C, 4S, 3S, 7H, QC"
        self.assertFalse(pc.Cards(input).is_two_pair())


class HandClassifyingTests(unittest.TestCase):

    def test_straight_flush(self):
        input = "JC, 10C, 8C, 7C, 9C"
        self.assertEqual(pc.STRAIGHT_FLUSH, pc.Cards(input).classify_hand())

    def test_four_of_a_kind(self):
        input = "7C, 7D, 8C, 7S, 7H"
        self.assertEqual(pc.FOUR_OF_A_KIND, pc.Cards(input).classify_hand())

    def test_full_house(self):
        input = "JC, JD, JS, 2C, 2S"
        self.assertEqual(pc.FULL_HOUSE, pc.Cards(input).classify_hand())

    def test_flush(self):
        input = "2C, 6C, 10C, KC, 3C"
        self.assertEqual(pc.FLUSH, pc.Cards(input).classify_hand())

    def test_straight(self):
        input = "3H, 5C, 4C, 2C, 6C"
        self.assertEqual(pc.STRAIGHT, pc.Cards(input).classify_hand())

    def test_three_of_a_kind(self):
        input = "KC, KD, KS, 7C, 6S"
        self.assertEqual(pc.THREE_OF_A_KIND, pc.Cards(input).classify_hand())

    def test_two_pair(self):
        input = "10C, 2S, 10H, 2D, 9C"
        self.assertEqual(pc.TWO_PAIR, pc.Cards(input).classify_hand())

    def test_pair(self):
        input = "JC, JD, 8S, 2C, 3H"
        self.assertEqual(pc.PAIR, pc.Cards(input).classify_hand())

    def test_high_card(self):
        input = "AS, 2H, 3S, 4C, 9D"
        self.assertEqual(pc.HIGH_CARD, pc.Cards(input).classify_hand())

class HandEqualityTests(unittest.TestCase):

    def test_unequal_hand_types(self):
        straight_flush = pc.Cards("KC, JC, QC, 9C, 10C")
        four_of_a_kind = pc.Cards("9D, 10S, 9S, 9C, 9H")
        full_house = pc.Cards("5C, 5S, 5H, 10S, 10D")
        flush = pc.Cards("2S, 3S, 4S, 5S, 8S")
        straight = pc.Cards("10S, 9D, 8S, 7S, 6S")
        three_of_a_kind = pc.Cards("KC, KD, KS, 7C, 6S")
        two_pair = pc.Cards("AC, AS, KH, KS, 2D")
        pair = pc.Cards("JC, JD, 8S, 2C, 3H")
        high_card = pc.Cards("AS, 2H, 3S, 4C, 9D")

        all_hands = [straight_flush, four_of_a_kind, full_house, flush, straight, three_of_a_kind, two_pair, pair, high_card]

        for hand in all_hands:
            rest_of_the_hands = [x for x in all_hands if hand != x]
            for other_hand in rest_of_the_hands:
                self.assertNotEqual(hand, other_hand)

    def test_equal_straight_flushes(self):
        input_a = "5S, 6S, 7S, 8S, 9S"
        input_b = input_a
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_straight_flushes(self):
        input_a = "5S, 6S, 7S, 8S, 9S"
        input_b = "5S, 6S, 7S, 8S, 4S"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_straight_flushes_with_different_suits(self):
        input_a = "5S, 6S, 7S, 8S, 9S"
        input_b = "9C, 5C, 8C, 6C, 7C"
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_four_of_a_kind(self):
        input_a = "8S, 8H, 8D, 8C, 2D"
        input_b = input_a
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_four_of_a_kind_group(self):
        input_a = "8S, 8H, 8D, 8C, 2D"
        input_b = "7S, 7H, 7D, 7C, 2D"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_four_of_a_kind_kicker(self):
        input_a = "8S, 8H, 8D, 8C, 2D"
        input_b = "8S, 8H, 8D, 8C, 5D"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_full_house(self):
        input_a = "4D, 2S, 4H, 2C, 4C"
        input_b = input_a
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_full_house_threes(self):
        input_a = "4D, 2S, 4H, 2C, 4C"
        input_b = "7D, 2S, 7H, 2C, 7C"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_full_house_twos(self):
        input_a = "7D, 5S, 7H, 5C, 7C"
        input_b = "7D, 2S, 7H, 2C, 7C"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_full_house_different_suits(self):
        input_a = "7D, 5S, 7H, 5C, 7C"
        input_b = "7S, 5D, 7H, 5H, 7C"
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_flushes(self):
        input_a = "2S, 3S, 4S, 5S, 8S"
        input_b = input_a
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_flushes(self):
        input_a = "2S, 3S, 4S, 5S, 8S"
        input_b = "2S, 3S, 4S, 5S, 9S"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

        input_a = "2S, 3S, 4S, 6S, 9S"
        input_b = "2S, 3S, 4S, 5S, 9S"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_flushes_with_different_suits(self):
        input_a = "5S, 8S, 10S, QS, KS"
        input_b = "KC, QC, 10C, 5C, 8C"
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_straights(self):
        input_a = "8D, 9C, 10S, JS, QS"
        input_b = input_a
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_straights(self):
        input_a = "8D, 9C, 10S, JS, QS"
        input_b = "8D, 9C, 10S, JS, 7S"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_straights_different_suits(self):
        input_a = "8D, 9D, 10S, JH, QS"
        input_b = "8D, 9C, 10C, JS, QS"
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_three_of_a_kind(self):
        input_a = "KC, KD, KS, 7C, 6S"
        input_b = input_a
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_three_of_a_kind_different_suits(self):
        input_a = "KC, KD, KS, 7C, 6S"
        input_b = "KC, KH, KS, 7D, 6C"
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_three_of_a_kind(self):
        input_a = "KC, KD, KS, 7C, 6S"
        input_b = "QC, QD, QS, 7C, 6S"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_three_of_a_kind_because_of_kicker(self):
        input_a = "KC, KD, KS, 7C, 6S"
        input_b = "KC, KD, KS, 8C, 6S"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_two_pair_equality(self):
        input_a = "JC, JD, 8S, 8C, 3H"
        input_b = "8S, 8C, JC, JD, 3H"
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_two_pair_first_pair_different(self):
        input_a = "QC, QD, 8S, 8C, 3H"
        input_b = "8S, 8C, JC, JD, 3H"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_two_pair_second_pair_different(self):
        input_a = "JC, JD, 8S, 8C, 3H"
        input_b = "4S, 4C, JC, JD, 3H"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_two_pair_kicker_different(self):
        input_a = "JC, JD, 8S, 8C, 7H"
        input_b = "8S, 8C, JC, JD, 3H"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_pairs(self):
        input_a = "JC, JD, 8S, 2C, 3H"
        input_b = input_a
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_pairs_different_suits(self):
        input_a = "JC, JD, 8S, 2C, 3H"
        input_b = "8C, 2D, JS, 3C, JH"
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_pairs(self):
        input_a = "JC, JD, 8S, 2C, 3H"
        input_b = "10C, 10D, 8S, 2C, 3H"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_pairs_because_of_kicker(self):
        input_a = "JC, JD, 8S, 2C, 3H"
        input_b = "10C, 10D, 9S, 2C, 3H"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_high_card(self):
        input_a = "2S, 6H, 3D, 5S, JD"
        input_b = input_a
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_unequal_high_cards(self):
        input_a = "2H, 3C, 4S, 5S, 8D"
        input_b = "2H, 3C, 4S, 5S, 9D"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

        input_a = "2H, 3C, 4S, 6S, 9D"
        input_b = "2H, 3C, 4S, 5S, 9D"
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_equal_high_cards_with_different_suits(self):
        input_a = "5H, 8S, 10D, QS, KC"
        input_b = "KC, QH, 10H, 5D, 8C"
        self.assertEqual(pc.Cards(input_a), pc.Cards(input_b))

class HandSortingTests(unittest.TestCase):

    def test_all_different_types(self):
        straight_flush = pc.Cards("KC, JC, QC, 9C, 10C")
        four_of_a_kind = pc.Cards("9D, 10S, 9S, 9C, 9H")
        full_house = pc.Cards("5C, 5S, 5H, 10S, 10D")
        flush = pc.Cards("2S, 3S, 4S, 5S, 8S")
        straight = pc.Cards("10S, 9D, 8S, 7S, 6S")
        three_of_a_kind = pc.Cards("KC, KD, KS, 7C, 6S")
        two_pair = pc.Cards("AC, AS, KH, KS, 2D")
        pair = pc.Cards("JC, JD, 8S, 2C, 3H")
        high_card = pc.Cards("AS, 2H, 3S, 4C, 9D")

        all_hands = [straight_flush, four_of_a_kind, full_house, flush, straight, three_of_a_kind, two_pair, pair, high_card]

        input = [flush, straight_flush, straight, pair, four_of_a_kind, full_house, two_pair, three_of_a_kind, high_card]
        expected = [straight_flush, four_of_a_kind, full_house, flush, straight, three_of_a_kind, two_pair, pair, high_card]

        self.assertEqual(expected, sorted(input, reverse=True))

    def test_flushes(self):
        flush_a = pc.Cards("5S, 8S, 10S, QS, KS")
        flush_b = pc.Cards("KC, QC, 10C, 5C, 8C")
        flush_c = pc.Cards("2S, 3S, 4S, 5S, 9S")
        flush_d = pc.Cards("2S, 3S, 4S, 6S, 9S")
        flush_e = pc.Cards("2S, 3S, 4S, 5S, 8S")

        input = [flush_d, flush_b, flush_e, flush_c, flush_a]
        expected = [flush_b, flush_a, flush_d, flush_c, flush_e]

        self.assertEqual(expected, sorted(input, reverse=True))

    def test_three_of_a_kind(self):
        hand_a = pc.Cards("KC, KD, KS, KH, 6S")
        hand_b = pc.Cards("KC, KD, KS, 8C, 6S")
        hand_c = pc.Cards("KC, KD, KS, 7C, 6S")
        hand_d = pc.Cards("KC, KH, KS, 7D, 6C")
        hand_e = pc.Cards("QC, QD, QS, 7C, 6S")

        input = [hand_e, hand_c, hand_a, hand_d, hand_b]
        expected = [hand_a, hand_b, hand_c, hand_d, hand_e]

        self.assertEqual(expected, sorted(input, reverse=True))

    def test_regression_where_four_of_a_kind_loses(self):
        input_a = "KC, KD, KS, KH, 6S"
        input_b = "KC, KD, KS, 8C, 6S"
        self.assertGreater(pc.Cards(input_a), pc.Cards(input_b))
        self.assertNotEqual(pc.Cards(input_a), pc.Cards(input_b))

    def test_pairs(self):
        hand_a = pc.Cards("JC, JD, JS, 8C, 3H")
        hand_b = pc.Cards("JS, JD, 8S, 8C, 3H")
        hand_c = pc.Cards("8S, 8C, JC, JD, 3H")
        hand_d = pc.Cards("JC, JD, 8S, 2C, 3H")
        hand_e = pc.Cards("8C, 2D, JS, 3C, JH")
        hand_f = pc.Cards("10C, 10D, 9S, 2C, 3H")
        hand_g = pc.Cards("10C, 10D, 8S, 2C, 3H")

        input = [hand_f, hand_e, hand_b, hand_d, hand_c, hand_a, hand_g]
        expected = [hand_a, hand_b, hand_c, hand_e, hand_d, hand_f, hand_g]

        self.assertEqual(expected, sorted(input, reverse=True))

    def test_high_cards(self):
        hand_a = pc.Cards("5H, 8S, 10D, QS, KC")
        hand_b = pc.Cards("KC, QH, 10H, 5D, 8C")
        hand_c = pc.Cards("2S, 6H, 3D, 5S, JD")
        hand_d = pc.Cards("2H, 3C, 4S, 6S, 9D")
        hand_e = pc.Cards("2H, 3C, 4S, 5S, 9D")
        hand_f = pc.Cards("2H, 3C, 4S, 5S, 8D")

        input = [hand_e, hand_d, hand_c, hand_f, hand_a, hand_b]
        expected = [hand_a, hand_b, hand_c, hand_d, hand_e, hand_f]

        self.assertEqual(expected, sorted(input, reverse=True))

class FindWinnerTests(unittest.TestCase):

    def test_all_types(self):
        input = ["Mr. Flush, 2S, 3S, 4S, 5S, 8S", "Mr. Three, KC, KD, KS, 7C, 6S", "Mr. Pair, JC, JD, 8S, 2C, 3H", "Mr Loser, AS, 2H, 3S, 4C, 9D"]
        expected = ["Mr. Flush"]
        winners = pc.find_winners(input)

        self.assertEqual(expected, winners)

    def test_multiple_winners(self):
        input = ["Mr. Flush, 2S, 3S, 4S, 5S, 8S", "Mr. Pair, JC, JD, 8S, 2C, 3H", "Mr. Just as good Flush, 2C, 3C, 4C, 5C, 8C", "Mr Loser, AS, 2H, 3S, 4C, 9D"]
        expected = ["Mr. Flush", "Mr. Just as good Flush"]
        winners = pc.find_winners(input)

        self.assertEqual(expected, winners)

def main():
    unittest.main()

if __name__ == '__main__':
    main()
